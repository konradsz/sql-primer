--
SELECT e.MemberID
FROM Entry e
WHERE e.TourID = 1 OR e.TourID = 2 OR e.TourID = 4;

-- More convenient is to use IN clause

SELECT e.MemberID
FROM Entry e
WHERE e.TourID IN (1, 2, 4);

-- Using NOT and IN

SELECT e.MemberID
FROM Entry e
WHERE e.TourID NOT IN (3, 5);

-- Set of IDs for Open tournament
SELECT t.TourID
FROM Tournament t
WHERE t.TourType = 'Open';

-- Use subquery to populate IN clause
SELECT e.MemberID
FROM Entry e
WHERE e.TourID IN (
  -- Subquery returns IDs of Open tournaments
  SELECT t.TourID
  FROM Tournament t
  WHERE t.TourType = 'Open');

-- Another way to get the same results as above but by using join and filtering
SELECT e.MemberID
FROM Entry e INNER JOIN Tournament t ON e.TourID = t.TourID
WHERE t.TourType = 'Open';

-- What are the names of all members who have ever entered any tournament?
SELECT m.LastName, m.FirstName
FROM Member m
WHERE EXISTS
(SELECT * FROM Entry e WHERE e.MemberID = m.MemberID);

--  members who have not entered a tournament
SELECT m.Lastname, m.FirstName
FROM Member m
WHERE NOT EXISTS
(SELECT * FROM Entry e WHERE e.MemberID = m.MemberID);

-- Members that have not entered Open tournament
SELECT m.Lastname, m.FirstName
FROM Member m
WHERE NOT EXISTS
(SELECT * FROM Entry e, Tournament t
    WHERE m.MemberID = e.MemberID
          AND e.TourID = t.TourID AND t.TourType = 'Open');

-- members with a handicap of less than 16
SELECT *
FROM Member m
WHERE m.Handicap < 16;

-- find all the members with a handicap less than
SELECT *
FROM Member m
WHERE Handicap <
      (SELECT Handicap
       FROM Member
       WHERE LastName = 'Burton' AND FirstName = 'Sandra');

-- find all the members who have a handicap less than the average
SELECT *
FROM Member m
WHERE m.Handicap <
      (SELECT AVG(Handicap)
       FROM Member);

-- see whether any junior members have a lower handicap than the average for seniors
SELECT *
FROM Member m
WHERE m.MemberType = 'Junior' AND Handicap < (
  SELECT AVG(Handicap)
  FROM Member
  WHERE MemberType = 'Senior');