-- Find people that have coaches
SELECT *
FROM Member m INNER JOIN Member c ON m.Coach = c.MemberID;

-- What are the names of coaches
SELECT DISTINCT c.FirstName, c.LastName
FROM Member m INNER JOIN Member c ON m.Coach = c.MemberID;

-- Who Is Being Coached by Someone with a Higher Handicap?
SELECT *
FROM Member m INNER JOIN Member c ON m.Coach = c.MemberID
WHERE m.Handicap < c.Handicap;

-- List the Names of All Members and the Names of Their Coaches
SELECT m.LastName AS MemberLast, m.FirstName AS MemberFirst,
       c.LastName AS CoachLast, c.FirstName AS CoachFirst
FROM Member m LEFT OUTER JOIN Member c ON m.Coach = c.MemberID;

-- Who coaches Mr Pollard
SELECT c.FirstName, c.LastName
FROM Member m, Member c
WHERE c.MemberID = m.Coach AND m.LastName = 'Pollard';


-- Finding members who are coached by someone with a higher handicap
-- Is coach is 'weaker' player than person being coached?
SELECT m.FirstName, m.LastName
FROM Member m, Member c
WHERE c.MemberID = m.Coach AND m.Handicap < c.Handicap;

-- Finding members who are coached by someone with a lower handicap
SELECT m.FirstName, m.LastName
FROM Member m, Member c
WHERE c.MemberID = m.Coach AND m.Handicap > c.Handicap;

-- Find members that entered two tournaments (both tournament number 2 and 3)
SELECT e1.MemberID
FROM Entry e1, Entry e2
WHERE e1.MemberID = e2.MemberID
      AND e1.TourID = 2 AND e2.TourID = 3;

-- Another way to find members that entered two tournaments (both tournament number 2 and 3)
SELECT e1.MemberID
FROM Entry e1 INNER JOIN Entry e2 ON e1.MemberID = e2.MemberID
WHERE e1.TourID = 2 AND e2.TourID = 3;