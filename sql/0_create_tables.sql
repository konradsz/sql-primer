CREATE TABLE Team (
  TeamName      TEXT UNIQUE,
  PracticeNight TEXT
);

CREATE TABLE Member (
  MemberID   SERIAL PRIMARY KEY,
  FirstName  TEXT,
  LastName   VARCHAR(20),
  Age        INTEGER NOT NULL,
  JoinDate   TIMESTAMPTZ DEFAULT current_timestamp,
  MemberType TEXT,
  Phone      TEXT,
  Handicap   INTEGER,
  Coach      INTEGER REFERENCES Member(MemberID),
  Team       TEXT REFERENCES Team(TeamName),
  Gender     VARCHAR(1) NOT NULL
);

ALTER TABLE Team ADD COLUMN Manager INTEGER REFERENCES Member(MemberID);

CREATE TABLE Tournament (
  TourID   SERIAL PRIMARY KEY,
  TourName TEXT,
  TourType TEXT
);

CREATE TABLE Entry (
  MemberID INTEGER REFERENCES Member(MemberID),
  TourID   INTEGER REFERENCES Tournament(TourID),
  Year     INTEGER
);

CREATE TABLE Type (
  Type TEXT UNIQUE,
  Fee  INTEGER
);