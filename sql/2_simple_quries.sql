-- The SQL for the query to retrieve Senior members is as follows:

SELECT *
FROM Member
WHERE MemberType = 'Senior';

-- The SQL to retrieve the name and phone columns from the Member table is:
SELECT LastName, FirstName, Phone
FROM Member;


SELECT LastName, FirstName, Phone
FROM Member
WHERE MemberType = 'Senior';

-- Using alias for table name

SELECT m.LastName, m.FirstName, m.Phone
FROM Member m
WHERE m.MemberType = 'Senior';

-- Using sql funcions e.g. upper
SELECT *
FROM Member m
WHERE UPPER(m.MemberType) = 'JUNIOR';

-- Logical AND operator
SELECT *
FROM Member m
WHERE m.MemberType = 'Junior' AND m.Gender = 'F';

-- Finding nulls
SELECT *
FROM Member m
WHERE NOT (m.Handicap IS NULL);

-- Finding unique member types
SELECT DISTINCT m.MemberType
FROM Member m;

-- Ordering
SELECT *
FROM Member m
ORDER BY m.LastName;

-- Ordering with filtering in where clause
SELECT *
FROM Member m
WHERE m.MemberType = 'Senior'
ORDER BY m.LastName, m.FirstName;

-- Descending ordering
SELECT m.Lastname, m.FirstName, m.Handicap
FROM Member m
ORDER BY m.Handicap DESC;


SELECT m.LastName, m.FirstName, m.Handicap
FROM Member m
ORDER BY (CASE
          WHEN m.Handicap IS NULL THEN 1
          ELSE 0
          END), m.Handicap;

-- Counting
SELECT COUNT(*) FROM Member m
WHERE m.MemberType = 'Senior';

SELECT COUNT(Handicap) FROM Member;

SELECT COUNT(DISTINCT MemberType) FROM Member;

-- Find members that entered particular tournament at certain year:
SELECT e.MemberID
FROM Entry e
WHERE e.TourID = 2 AND e.Year = 2017;

