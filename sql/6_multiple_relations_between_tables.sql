-- Get Team info
SELECT m.MemberID, m.LastName, m.FirstName, m.Team,
  t.TeamName, t.PracticeNight, t.Manager
FROM Member m INNER JOIN Team t ON m.Team = t.TeamName;

-- Team info with Manager info
SELECT t.TeamName, t.PracticeNight, t.Manager,
  m.MemberID, m.LastName AS ManagerNname, m.FirstName As ManagerSurname
FROM Team t INNER JOIN Member m ON t.Manager = m.MemberID;

-- Show the members who manage the team they are in
SELECT *
FROM Member m  INNER JOIN Team t
    ON t.TeamName = m.Team AND m.MemberID = t.Manager;

-- The query that follows will provide the information about the members, their teams, and the managers’ IDs (t.Manager);
--  however, it does not provide the managers’ names
SELECT m.MemberID, m.LastName, m.FirstName, t.TeamName, t.Manager
FROM Member m INNER JOIN Team t ON m.Team = t.TeamName;

-- Info about member, team he/she belongs to and manager that manages his/her team
SELECT *
FROM (Member m INNER JOIN Team t ON m.Team = t.TeamName)
  INNER JOIN Member m2 ON t.Manager = m2.MemberID;

-- pick a member and find out what team he or she is on and who the manager is for that team
SELECT m.LastName, m.FirstName, m.Team, m2.LastName AS ManagerName, m2.FirstName AS ManagerSurname
FROM Member m, Team t, Member m2
WHERE m.Team = t.TeamName AND t.Manager = m2.MemberID;

-- Find teams whose managers are not members of the team
SELECT t.teamname
FROM Member m, Team t
WHERE m.MemberID = t.Manager
      AND (m.Team <> t.Teamname OR m.Team IS NULL);

-- Another way to find teams whose managers are not members of the team
SELECT t.teamname
FROM Member m INNER JOIN Team t ON m.MemberID = t.Manager
WHERE m.Team <> t.Teamname OR m.Team IS NULL;