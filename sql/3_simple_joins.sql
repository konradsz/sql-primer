-- Cartesian join
SELECT *
FROM Member m CROSS JOIN Type t;

-- Another way to write Cartesian join
SELECT *
FROM Member m, Type t;

-- Joining on MemberType
SELECT *
FROM Member m INNER JOIN Type t ON m.MemberType = t.Type;

--  Another way to write inner join on MemberType
SELECT *
FROM Member m, Type t
WHERE m.MemberType = t.Type;

-- Find info about people that ever entered any tournament
SELECT *
FROM (Member m INNER JOIN Entry e ON m.MemberID = e.MemberID)
  INNER JOIN Tournament t ON e.TourID = t.TourID;

-- Find all people that entered London tournament in 2016
SELECT LastName, FirstName
FROM (Member m INNER JOIN Entry e ON m.MemberID = e.MemberID)
  INNER JOIN Tournament t ON e.TourID = t.TourID
WHERE TourName = 'London'
      AND Year = 2016;

-- Another way to Find all people that entered London tournament in 2016
SELECT m.LastName, m.FirstName
FROM Member m, Entry e, Tournament t
WHERE m.MemberID = e.MemberID
      AND e.TourID = t.TourID
      AND t.TourName = 'London' AND e.Year = 2016;

-- Include Sarah Beck in Members list
SELECT *
FROM Member m LEFT OUTER JOIN Type t ON m.MemberType = t.Type;

-- Right join
SELECT *
FROM Type t RIGHT OUTER JOIN Member m ON m.MemberType = t.Type;

-- Full join
SELECT *
FROM Member m FULL OUTER JOIN Type t ON m.MemberType = t.Type;