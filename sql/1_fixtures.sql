INSERT INTO Team (TeamName, PracticeNight) VALUES
  ('League of Villains', 'Monday'),
  ('Avengers', 'Wednesday'),
  ('Social', 'Friday'),
  ('Beginners', 'Saturday');

INSERT INTO Member (FirstName, LastName, Age, MemberType, Handicap, Team, Gender, Coach) VALUES
  ('Brenda', 'Nolan', 30, 'Senior', 0, 'Avengers', 'F', NULL),
  ('William', 'Cooper', 39, 'Senior', 1, 'League of Villains', 'M', 1),
  ('Robert', 'Pollard', 40, 'Social', 6, 'Avengers', 'M', 2),
  ('Melisa', 'McKenzie', 22, 'Senior', 3, 'Avengers', 'F', 1),
  ('Thomas', 'Spence', 24, 'Senior', 4, 'League of Villains', 'M', NULL),
  ('Sandra', 'Burton', 26, 'Junior', 5, 'Social', 'F', 1),
  ('Helen', 'Branch', 28, 'Social', 3, 'Social', 'F', NULL),
  ('Sarah', 'Beck', 35, NULL , 0, 'League of Villains', 'F', NULL),
  ('Daniel', 'Wilcox', 23, 'Junior', 10, 'Beginners', 'M', NULL),
  ('Barbara', 'Olson', 22, 'Junior', 11, 'Beginners', 'F', NULL),
  ('Jane', 'Gilmore', 26, 'Junior', 1, 'Avengers', 'F', 4),
  ('Thomas', 'Sexton', 50, 'Senior', 3, NULL, 'M', 5);

-- Add Manager to Team
UPDATE Team SET Manager = (SELECT MemberID from Member WHERE LastName = 'Cooper') where TeamName = 'League of Villains';
UPDATE Team SET Manager = (SELECT MemberID from Member WHERE LastName = 'Sexton') where TeamName = 'Avengers';
UPDATE Team SET Manager = (SELECT MemberID from Member WHERE LastName = 'Gilmore') where TeamName = 'Beginners';
UPDATE Team SET Manager = (SELECT MemberID from Member WHERE LastName = 'Spence') where TeamName = 'Social';

INSERT INTO Tournament (TourName, TourType) VALUES ('London', 'Open'), ('Manila', 'Open'), ('Warsaw', 'Social'), ('Paris', 'Open'), ('Tokyo', 'Social');

INSERT INTO entry (MemberID, TourID, Year) VALUES
  (2, 1, 2015), (2, 1, 2016), (2, 1, 2017), (2, 1, 2018), (2, 2, 2015), (2, 2, 2018), (2, 3, 2016), (2, 4, 2017),
  (3, 5, 2018),
  (4, 1, 2015), (4, 1, 2016), (4, 2, 2017), (4, 3, 2016), (4, 3, 2017), (4, 4, 2017), (4, 4, 2018),
  (5, 1, 2016), (5, 2, 2015), (5, 3, 2016), (5, 3, 2017), (5, 4, 2015), (5, 4, 2018);

INSERT INTO type VALUES ('Associate', 60), ('Junior', 150), ('Senior', 300), ('Social', 50);